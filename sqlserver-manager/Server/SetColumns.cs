﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Maticsoft.Model;

namespace Server
{
    public partial class SetColumns : Office2007Form
    {
        public SetColumns()
        {
            InitializeComponent();
        }
        //窗体加载事件
        private void SetColumns_Load(object sender, EventArgs e)
        {
            listnew.Items.Clear();
            listold.Items.Clear();
            ListBing();
            Columns.ary_col.Clear();
        }
        //数据绑定
        public void ListBing()
        {
            try
            {
                foreach (string s in Columns.ary_col)
                {
                    listold.Items.Add(s);
                }
                listold.SelectedItem = null;
            }
            catch { }
        }
        //添加事件
        private void qu_Click(object sender, EventArgs e)
        {
            try
            {
                 foreach(Object l in listold.SelectedItems)
                 {
                     if (!listnew.Items.Contains(l)) listnew.Items.Add(l);
                 }
            }
            catch { }
        }
        //添加所有事件
        private void quall_Click(object sender, EventArgs e)
        {
            try
            {
                listnew.Items.Clear();
                listnew.Items.AddRange(listold.Items);
            }
            catch { }
        }
        //移出事件
        private void lai_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < listnew.SelectedItems.Count;)
                {
                    listnew.Items.Remove(listnew.SelectedItems[0]);
                }
            }
            catch { }
        }
        //移出所有
        private void laiall_Click(object sender, EventArgs e)
        {
            try
            {
                listnew.Items.Clear();
            }
            catch { }
        }
        //保存事件
        private void buttonX1_Click(object sender, EventArgs e)
        {
            try
            {
                Columns.SetColumns(listnew);
                this.Close();
            }
            catch { }
        }
    }
}
